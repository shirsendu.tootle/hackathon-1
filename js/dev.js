function getCounts(str) {
    letterCount = {};
    for (let letter of str) {
        letterCount[letter] ??= 0;
        letterCount[letter]++;
    }
    return letterCount;
}

function anagrams(str, arr) {
    counts = getCounts(str);
    return arr.filter(anagram =>
        anagram.length === str.length &&
        Object.entries(getCounts(anagram)).every(([letter, count]) =>
            counts[letter] === count
        )
    );
}

$(document).ready(function() {
    $(document).on('click', '.ajaxBtn', function(e) {
        var string = $(document).find('.string').val();

        var result = 'Fetching the result...';
        $(document).find('.resultblock').html(result);

        var url = "dictionary.json";         
        $.getJSON(url, function (json) {
            var array = [];
            for (var key in json) {
                array.push(key);                
            }
            result = anagrams(string, array); 
            console.log(result);
            if(result == '') {
                result = 'Sorry, No result found';
            } else {
                result = result.join(', ');
            }
            $(document).find('.resultblock').html(result);
        });
    });
});


